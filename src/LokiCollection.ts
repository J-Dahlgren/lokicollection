import { loki } from "./loki";
import { BehaviorSubject, Subject } from "rxjs";

export class LokiCollection<T extends object>{
    protected datastream: BehaviorSubject<loki<T>[]> = new BehaviorSubject<loki<T>[]>([]);
    protected deleteEvent: Subject<number> = new Subject<number>();
    protected addEvent: Subject<loki<T>> = new Subject<loki<T>>();
    protected updateEvent: Subject<loki<T>> = new Subject<loki<T>>();
    private BlockEvents = false;

    public get Stream() {
        return this.datastream.asObservable();
    }
    public get DeleteEvent() {
        return this.deleteEvent.asObservable();
    }
    public get AddEvent() {
        return this.addEvent.asObservable();
    }
    public get UpdateEvent() {
        return this.updateEvent.asObservable();
    }
    constructor(protected db: Loki, protected collectionName: string) {
        this.Collection.on(["insert", "update", "delete"], () => {
            if (!this.BlockEvents) this.datastream.next(this.data);
        });
        this.Collection.on(["delete"], (RemovedElements: loki<T> | loki<T>[]) => {
            if (!this.BlockEvents) {
                if (Array.isArray(RemovedElements)) {
                    for (const removed of RemovedElements) {
                        this.deleteEvent.next(removed.$loki);
                    }
                } else {
                    this.deleteEvent.next(RemovedElements.$loki);
                }
            }
        });
        this.Collection.on(["insert"], (AddedElements: loki<T> | loki<T>[]) => {
            if (!this.BlockEvents) {
                if (Array.isArray(AddedElements)) {
                    for (const added of AddedElements) {
                        this.addEvent.next(added);
                    }
                } else {
                    this.addEvent.next(AddedElements);
                }
            }
        });
        this.Collection.on(["update"], (UpdatedElements: loki<T> | loki<T>[]) => {
            if (!this.BlockEvents) {
                if (Array.isArray(UpdatedElements)) {
                    for (const updated of UpdatedElements) {
                        this.updateEvent.next(updated);
                    }
                } else {
                    this.updateEvent.next(UpdatedElements);
                }
            }
        });
    }

    public get Collection() {
        const collection: Collection<T> | null = this.db.getCollection<T>(this.collectionName);
        if (!collection) {
            return this.db.addCollection<T>(this.collectionName, { disableMeta: true });
        }
        return collection;
    }
    public get data() {
        return this.Collection.data as loki<T>[];
    }

    /**
     * Blocks any reported changes to the stream until the action is c ompleted, pushes the collection after finished
     * @param CB Action to be performed
     */
    public PerformBlockingAction(CB: () => void) {
        this.BlockEvents = true;
        try {
            CB();
        } catch (error) {

        }
        this.BlockEvents = false;
        this.datastream.next(this.data); // Make sure any changes are pushed to clients
    }
}