import * as Loki from "lokijs";

export function InitLoki(fileName: string, persist: boolean = true) {
    return new Promise<Loki>((resolve, reject) => {
        const autoloadCallback = (err?: any) => {
            if (err) {
                reject(err);
            } else {
                resolve(db);
            }
        };
        const db: Loki = new Loki.default(fileName, {
            autoload: persist,
            autosave: persist,
            autosaveInterval: persist ? 5000 : undefined,
            autoloadCallback: persist ? autoloadCallback : undefined
        });
        if (!persist) {
            resolve(db);
        }
    });
}
