module.exports = {
    preset: 'ts-jest',
    globals: {
        "ts-jest": {
            tsConfig: 'tsconfig.json',
            diagnostics: false
        },
    },
    testEnvironment: 'node',
    verbose: true,
    collectCoverage: true
};
