
import { InitLoki, LokiCollection, loki } from "../src";
const Sleep = (millis: number) => new Promise(resolve => setTimeout(() => resolve(), millis));
describe("LokiDB", () => {
    it("Creates collection", async () => {
        const db = await InitLoki("", false);
        const col = new LokiCollection<{}>(db, "test");
        const mockFn = jest.fn();
        col.Stream.subscribe(v => mockFn());
        expect(col).toBeTruthy();
        expect(col.Collection).toBeTruthy();
        expect(col.data.length).toBe(0);
        expect(mockFn).toBeCalledTimes(1);
        col.Collection.insertOne({});
        await Sleep(10);
        expect(mockFn).toBeCalledTimes(2);
        col.PerformBlockingAction(() => {
            for (let i = 0; i < 10; i++) {
                col.Collection.insertOne({});
            };
        });
        await Sleep(10);
        expect(mockFn).toBeCalledTimes(3);
        expect(col.data.length).toBe(11);
        const mockFn2 = jest.fn();
        col.AddEvent.subscribe(x => mockFn2());
        col.DeleteEvent.subscribe(x => mockFn2());
        const x: loki<{}> = col.Collection.insertOne({});
        col.Collection.remove(x.$loki);
        expect(col.data.length).toBe(11);
        expect(mockFn2).toBeCalledTimes(2);

    });
    it("Emits CRUD Events", async () => {
        const db = await InitLoki("", false);
        const col = new LokiCollection<any>(db, "test");
        const collection = col.Collection;
        const addFn = jest.fn();
        const updateFn = jest.fn();
        const deleteFn = jest.fn();
        col.AddEvent.subscribe(e => addFn());
        col.DeleteEvent.subscribe(e => deleteFn());
        col.UpdateEvent.subscribe(e => updateFn());
        let first = collection.insertOne({});
        first.test = "123";
        first = collection.update(first);
        collection.remove(first);
        expect(col.data.length).toBe(0);
        expect(addFn).toBeCalledTimes(1);
        expect(deleteFn).toBeCalledTimes(1);
        expect(updateFn).toBeCalledTimes(1);

    });
});